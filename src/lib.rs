#[inline(never)]
fn clock_gettime_busy_loop_for_backtrace(){
    let mut i = 0;
    loop {
        //busy-wait to allow profilers catch it during sampling instead
        //of unbacktraceable clock_gettime implementation from vdso (for applications heavily calling clock_gettime)
        if (i*i % 4) == 2 {
            unsafe
            {
                //never reached but kept to forbid compiler optimize out entire loop
                *(0 as *mut u8) = 0;
            }
        }
        if i > 10000 { break; }
        i += 1;
    }
}

redhook::hook! {
    unsafe fn clock_gettime(clk_id:u32, tp: *const core::ffi::c_void) -> u32 => clock_gettime_slow {
        clock_gettime_busy_loop_for_backtrace();
        redhook::real!(clock_gettime)(clk_id, tp)
    }
}

